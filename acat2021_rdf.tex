\documentclass[a4paper]{jpconf}
\usepackage{import}
\usepackage{graphicx}
\usepackage{minted}
\usepackage{hyperref}
\graphicspath{{figures/}}
\begin{document}

\title{RDataFrame enhancements for HEP analyses}

\author{E Guiraud$^1$, J Blomer$^1$,
S Hageboeck$^2$, A Naumann$^1$, V E Padulano$^1$, E Tejedor$^1$, S Wunsch$^1$}
\address{$^1$ROOT team, EP-SFT, CERN}
\address{$^2$IT-SC-RD, CERN}
\ead{enrico.guiraud@cern.ch}

\begin{abstract}
In recent years, RDataFrame, ROOT's high-level interface for data analysis and processing, has seen widespread adoption
on the part of HEP physicists. Much of this success is due to RDataFrame's ergonomic programming model that enables the
implementation of common analysis tasks more easily than previous APIs, without compromising on application performance.
Nonetheless, RDataFrame's interfaces have been further improved by the recent addition of several major HEP-oriented
features: in this contribution we will introduce for instance a dedicated syntax to define systematic variations,
per-data-sample call-backs useful to define quantities that vary on a per-sample basis, simplifications of collection
operations and the injection of just-in-time-compiled Python functions in the optimized C++ event loop.
\end{abstract}

\section{Introduction}

RDataFrame (\cite{tdf}, \cite{rdfchep2018}) is an efficient and ergonomic interface for HEP analysis tasks, in C++ and Python.
Thanks to its API inspired by declarative programming principles (users state what results they want to obtain from a dataset,
the system decides how computations are scheduled), the same high-level programming model covers a large variety of use cases:
from the production of few histograms to thousands, from quick data exploration performed on a laptop to realistic
analysis applications on many-core machines (\cite{elithesis}), to distributed execution on a cluster (\cite{distrdf}).
Listing \ref{list:rdfexample} presents a simple example usage.

Since its introduction in ROOT (\cite{ROOT}) in version 6.14, RDataFrame has seen widespread usage, and with that usage
came valuable user feedback. Physicists are applying RDataFrame to more and more complex use cases, they employ it as the
foundation for more specialized frameworks (e.g.\ \cite{bamboo}, \cite{CROWN}) as well as dataset-to-dataset transformation
tools, and require seamless integration of RDataFrame data processing with Python machine learning frameworks and other
tools from the Python data science ecosystem.
%
In time, RDataFrame also became a user-friendly point of entry to modern ROOT features (see Fig.
\ref{fig:rdf_ecosystem}). This was not a role that was initially foreseen for this interface, and it provides additional
requirements that are influencing its evolution.

This work discusses several novel RDataFrame features introduced in ROOT v6.26 that address the most common user
requirements emerging from recent feedback. Most notably, a dedicated syntax to express systematic variations is
introduced in Sec. \ref{sec:vary}: it enables physicists to express systematics in an ergonomic fashion, without the
burden of the related book-keeping, while fitting naturally with the rest of the programming model.

\begin{figure}[t]
\begin{center}
\includegraphics[width=\textwidth]{rdf_ecosystem.png}
\caption{RDataFrame acts as a natural high-level entry point for many ROOT features, including recent developments such
   as RNTuple (\cite{rntuple}) and SOFIE (a fast machine learning inference engine, \cite{SOFIE}).}
\label{fig:rdf_ecosystem}
\end{center}
\end{figure}

\begin{listing}
\begin{minted}{python}
ROOT.EnableImplicitMT() # enable multi-threading in ROOT
df = ROOT.RDataFrame(treeName="Events", filesList)
df = df.Filter("pts[abs(eta) < 1].size() > 0")
       .Define("myVec", myFunctor(), ["pts"])
h = df.Histo1D("myVec")
# write events that pass the Filter to a new tree (including column "myVec")
df.Snapshot("newtree", "newfile.root")
\end{minted}
\caption{These few lines of Python code produce a skimmed dataset with an additional derived quantity and a control plot in a single multi-thread event loop.}
\label{list:rdfexample}
\end{listing}

\section{Definition of per-sample values}

HEP analyses must often treat data pertaining to different data taking periods slightly differently.
Similarly, Monte Carlo samples might require different event weights than actual data.
At the same time, in order to reduce the size of the dataset, the metadata useful to distinguish different samples
is typically not part of the dataset itself, e.g.\ it might be stored in a small, separate dataset distributed together with the main one.

One way to address these scenarios with RDataFrame is to build different computation graphs for different samples: these
graphs will look exceedingly similar, except for the nodes that need to deal with these differences in treatment of
different sample types. \texttt{DefinePerSample} solves this problem more elegantly, enabling users to process different samples slightly differently in the same computation graph. Just like a \texttt{Define}, this transformation creates a new
logical column holding the result of a user-defined callable invocation, with two
important differences: firstly, the input to the callable are not other dataset columns,
but rather metadata information regarding the sample being processed (e.g.\ the name of the tree, the file, the range of
entries being processed in the form of a \texttt{RSampleInfo} object); secondly, the callable is not invoked at every event,
but only once at the beginning of the processing of a new sample (e.g.\ when a single-thread event loop switches files or
when a new multi-thread task starts executing). This second property makes \texttt{DefinePerSample} a useful tool also
for injecting callbacks that can execute less frequently than at every entry and that need information on the data being
processed: for example this is an appropriate hook for the injection of a progress bar display callback. Listing
\ref{list:definepersample} shows an example invocation of \texttt{DefinePerSample}.

\begin{listing}[h]
\begin{minted}{cpp}
df.DefinePerSample("weight", [](unsigned slot, const RDF::RSampleInfo &s) {
  return s.Contains("MC") ? 0.5 : 1.; })
  .Histo1D("value","weight");
\end{minted}
\caption{This C++ code snippet defines different weights for Monte Carlo and data samples.}
\label{list:definepersample}
\end{listing}

\section{Column redefinition}

RDataFrame was initially designed as a high-level interface to select events and perform data reductions, but it soon
became obvious that the programming model was flexible enough to extend to other use cases, in particular dataset-to-dataset
transformations such as skims or dataset augmentations (e.g.\ addition of a derived column). In this context users might
need to apply corrections to some column values before writing out a modified dataset, but RDataFrame made it unnecessarily
cumbersome by completely disallowing column values from being overwritten: a \texttt{Define} always creates a new column,
with a different name.

The recently introduced \texttt{Redefine} method makes it possible to modify the value and/or type of a column before
further processing: this makes it easy to replace a single column in a large dataset, perform quick tests for the response
of a selection to the variation of variables, or apply corrections to certain quantities, e.g.\ in specialized
RDataFrame-based frameworks, before the dataframe object is further passed down to users. Debug printouts can also
be easily injected by redefining the column as itself, with an identity function that performs a printout as a side-effect.
Listing \ref{list:redefine} provides a usage example.

\begin{listing}[h]
\begin{minted}{c++}
df.Redefine("x", [](double x) { return float(x); }, {"x"})
  .Snapshot("tree", "newfile.root")
\end{minted}
\caption{An example invocation of \texttt{Redefine} in C++ that reduces the precision of column ``x'' and writes out a modified dataset.}
\label{list:redefine}
\end{listing}

\section{Systematic variations}
\label{sec:vary}

RDataFrame's goal is to accompany HEP physicists from the stage of quick data exploration to a full-blown, large-scale
analysis performing complex computations and requiring a large amount of computing resources. RDataFrame should make it
possible to organically grow analysis code step by step, introducing nuances and complexities incrementally, without
large code refactorings or changes of paradigm.

It is common for HEP analyses to eventually include the study of systematic variations. Handling systematic variations
in RDataFrame used to require a certain increase in the analysis code complexity, mainly due to the tedious manual
book-keeping required to keep track of the different results belonging to different systematics.

From the standpoint of a HEP physicist, the study of systematic variations involves many different, often conceptually complex cases.
From the standpoint of the pure numerical computation, however, what typically happens is that the application must produce
multiple results instead of a single one, each computed in a ``universe'' in which certain inputs take modified values.
Our challenge is therefore to a) offer a user-friendly user API that nevertheless allows users to express most or all use cases
for systematic variations present in HEP, and b) transparently propagate the variations through the RDataFrame computation
graph (including event/object selections and the computation of derived quantities) in order to produce the varied results.
In doing so, it is highly desirable to avoid replicating the whole computation graph, with its computations and the related
I/O operations, for each separate ``universe''.

Starting from ROOT v6.26, RDataFrame provides a flexible syntax to define systematic variations that aims to
satisfy these requirements. Listing \ref{list:vary} showcases the feature, which involves two new function calls:
\texttt{Vary} lets users register varied values for one or more existing columns (e.g.\ up/down variations for ``pt'' in the example),
while \texttt{VariationsFor} transforms the single ``nominal'' result into a dictionary-like object that contains results
for each of the variations. Note that all other RDataFrame code remains the same as for the nominal case, and
the presence of systematic variations is transparently propagated through \texttt{Filter}, \texttt{Define} and other calls.

\begin{listing}[h]
\begin{minted}{c++}
auto nominal_hx =
   df.Vary("pt", "ROOT::RVecD{pt*0.9, pt*1.1}", {"down", "up"})
     .Filter("pt > k")
     .Define("x", someFunc, {"pt"})
     .Histo1D<float>("x");

// request the generation of varied results from the nominal
RResultMap<TH1D> hx = ROOT::RDF::VariationsFor(nominal_hx)
hx["nominal"].Draw()
hx["pt:down"].Draw("SAME")
hx["pt:up"].Draw("SAME")
\end{minted}
\caption{An example usage of \texttt{Vary} and \texttt{VariationsFor}, in C++.}
\label{list:vary}
\end{listing}

Some aspects of this interface will be improved with the help of a first round of user feedback; in particular, we expect
to be able to further simplify the definition of simultaneous variations so that the explicit construction of nested arrays
can be avoided in the most common cases.

\subsection{Performance measurements}

A dedicated syntax to express systematic variations not only simplifies user code, but by
giving RDataFrame semantic information about user intention it allows certain
optimizations in the construction of its computation graph. This results in less overhead
during the event loop, as we can see from Fig. \ref{fig:benchmarks}.
The benchmark did not involve any disk I/O (entries were generated on the fly by RDataFrame, and consisted of a single floating point number) and it consisted in filling one or more histograms for the nominal case and multiple variations. We compare the performance of this task
without using \texttt{Vary} (which requires defining varied columns with \texttt{Define} and
booking the corresponding histograms in a for loop) and with \texttt{Vary}. The new syntax
offers a significant performance boost for the heavier use cases (B. and C. in the figure)
and it results in a small penalty when only one histogram with an up/down variation is filled.

\begin{figure}[h]
\begin{center}
\def\svgwidth{0.7\textwidth}
\import{./figures/}{bench.pdf_tex}
   \caption{Runtimes for RDataFrame event loops for three different scenarios: \textbf{A}. 100M events, 1 histogram produced, 2 variations (nominal, up, down histograms filled). \textbf{B}. 10M events, 1 nominal histogram, 100 variations (101 histograms filled). \textbf{C} 100k events, 20 nominal histograms, 100 variations (2k histograms filled).}
\label{fig:benchmarks}
\end{center}
\end{figure}

\section{Injecting Python functions into a C++ event loop via Numba}

Python is increasingly relevant for the HEP community, and particularly so in the area of analysis.
RDataFrame, like all of ROOT, offers dynamically generated Python bindings via PyROOT (\cite{pyroot}), while its internal
event loop remains in C++: this offers greater opportunities for performance optimizations and allows multi-thread parallelism
that would be more complicated in Python due to its Global Interpreter Lock (the \textit{GIL}).
At the same time, the Python ecosystem has developed tools to generate efficient machine code from Python code to speed
up numerical applications; the most interesting such tool for our purposes is Numba (\cite{numba}), which is able to
compile Python functions (that restrict themselves to use a specific subset of the language) to optimized machine code.
Numba can transform Python functions in compiled functions accessible as C function pointers that can be
made known to ROOT's C++ interpreter, Cling. The result is that RDataFrame can call these compiled Python functions from
the C++ event loop without fear of the GIL. With this system it is also possible to use array columns, that RDataFrame
exposes as the \texttt{RVec} type, as Numpy arrays in Python functions, with no copies in between: the Numpy arrays
will be initialized to act as a view on the contents of the \texttt{RVec}. Listing \ref{list:numba} shows how this
looks in practice.

\begin{listing}[h]
\begin{minted}{python}
@ROOT.Numba.Declare(["RVecD","RVecD"], "RVecD")
def good_pts(pts, etas):
    return pts[np.abs(etas) < 1]

df.Define("good_pts", "Numba::good_pts(pts, etas)")
\end{minted}
\caption{Through \texttt{Numba.Declare}, the \texttt{good\_pts} Python function is declared to Cling as a C function and can be used in RDataFrame as usual.}
\label{list:numba}
\end{listing}

Most of the boilerplate code above could be generated automatically by RDataFrame, including the input and output
types of the columns involved; therefore, in the future we expect to be able to simplify the API so that the code above will soon just be:

\begin{listing}[h]
\begin{minted}{python}
df.Redefine("pts", lambda pts, etas: pts[np.abs(etas) < 1])
\end{minted}
%\caption{}
\label{list:numba2}
\end{listing}

\section{Conclusions}

RDataFrame keeps improving to simplify the life of HEP physicists, thanks to valuable feedback from the user community.
Future work will introduce further enhancements, some of which are outlined in this work, as well as performance
optimizations of the data handling in the inner event loop and more Pythonic interfaces for common use cases.

\section*{References}
\begin{thebibliography}{9}
\bibitem{tdf} Guiraud E, Naumann A and Piparo D 2017 TDataFrame: functional chains for ROOT data analyses (v1.0), \textit{Zenodo}, https://doi.org/10.5281/zenodo.260230
\bibitem{rdfchep2018} Piparo D, Canal P, Guiraud E, Pla XV, Ganis G, Amadio G, Naumann A and Tejedor E. 2018 RDataFrame: Easy Parallel ROOT Analysis at 100 Threads, \textit{EPJ Web of Conferences 2019}, Vol. 214, p. 06029 EDP Sciences.
\bibitem{elithesis} Manca E Precision measurements of W detected at CMS \textit{Doctoral dissertation, Universita \& INFN Pisa (IT)}
\bibitem{distrdf} Padulano V E, Villanueva J C, Guiraud E and Saavedra E T  Distributed data analysis with ROOT RDataFrame \textit{EPJ Web of Conferences 2020} EDP Sciences Vol. 245, p. 03009
\bibitem{ROOT} Brun R and Rademakers F 1996 ROOT - An Object Oriented Data Analysis Framework, \textit{Proceedings AIHENP'96 Workshop}, Nucl. Inst. \& Meth. in Phys. Res. A 389 (1997) 81-86.
\bibitem{bamboo} David P Readable and efficient HEP data analysis with bamboo \textit{EPJ Web of Conferences 2021} EDP Sciences Vol. 251, p. 03052
\bibitem{CROWN} CROWN framework by KIT-CMS, \texttt{https://github.com/KIT-CMS/CROWN}
\bibitem{rntuple} Blomer J, Canal P, Naumann A and Piparo D Evolution of the ROOT Tree I/O \textit{EPJ Web of Conferences 2020} EDP Sciences, Vol. 245, p. 02030
\bibitem{SOFIE} An S and Moneta L. C++ Code Generation for Fast Inference of Deep Learning Models in ROOT/TMVA \textit{EPJ Web of Conferences 2021} EDP Sciences Vol. 251, p. 03040
\bibitem{pyroot} Galli M, Tejedor E and Wunsch S. A new PyROOT: Modern, interoperable and more pythonic. \textit{EPJ Web of Conferences 2020} EDP Sciences, Vol. 245, p. 06004
\bibitem{numba} Lam S K et al., DOI 10.5281/zenodo.4343230
\end{thebibliography}

%\section*{Appendix}
%\setcounter{section}{1}

\end{document}
